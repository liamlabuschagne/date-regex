#!/usr/bin/env python3
import sys
date = sys.argv[1]

leapyear = False


def invalid():
    print("Invalid")
    quit()


# split into day, month and year
try:
    date = date.split('/')
except:
    invalid()

if len(date) < 3:
    invalid()

day = int(date[0])
month = int(date[1])
year = int(date[2])


# check maximum limits
if day < 1 or day > 31 or month < 1 or month > 12 or year < 1:
    invalid()

# Check for leap year
if year % 4 == 0:
    leapyear = True
    if leapyear % 100 == 0:
        leapyear = False
        if leapyear % 400 == 0:
            leapyear = True

# Check for 30 day months
shortmonths = [4, 6, 9, 11]
if shortmonths.__contains__(month) and day > 30:
    invalid()

# Check February
if month == 2:
    if day > 29:
        invalid()
    # Check February with leap year
    if not leapyear and day > 28:
        invalid()

print("Valid date.")
